#!/bin/sh

scp box-streams.m3u tc@box:/tmp
ssh tc@box "sudo cp -v /tmp/box-streams.m3u /root/.mpd/playlists/\[Radio\ Streams\].m3u && filetool.sh -b"
