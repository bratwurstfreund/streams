#!/bin/sh

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

USER_AGENT="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:92.0) Gecko/20100101 Firefox/92.0"

for i in $(jq --raw-output '.[] | .stream' streams.json); do
  status_code=$(curl -H "User-Agent: ${USER_AGENT}" --location --head --write-out %{http_code} --silent --output /dev/null ${i})
  if [[ "$status_code" -eq 400 ]] ; then
    status_code=$(curl -H "User-Agent: ${USER_AGENT}" --max-time 2 --write-out %{http_code} --silent --output /dev/null ${i})
  fi
  if [[ "$status_code" -ne 200 ]] ; then
    echo "${RED}${status_code}${NC} ${i}"
  else
    echo "${GREEN}${status_code}${NC} ${i}"
  fi
done