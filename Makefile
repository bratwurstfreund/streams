.phony: all pyradio clean

all: pyradio box html

pyradio:
	$(shell jq -r '. | map([.name, .stream] | join(", ")) | join("\n")' streams.json > stations.csv)

box:
	$(shell jq -r '. | map([.stream, .name] | join("#")) | join("\n")' streams.json > box-streams.m3u)

html:
	$(shell echo "<html><head><title>Radio Streams</title></head><body><ul>$$(cat streams.json | jq -r '. | map(["<li><a href=\"", .stream, "\">", .name, "</a></li>"] | join("")) | join("\n")')</ul></body></html>" > streams.html)

test:
	./test.sh

clean:
	rm -f stations.csv box-streams.m3u streams.html